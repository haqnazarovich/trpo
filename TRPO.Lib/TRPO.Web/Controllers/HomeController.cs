﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using TRPO.Lib;

namespace TRPO.Web.Controllers
{
    public class HomeController : Controller
    {
        [HttpGet]
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult Index(string radius, string nka)
        {
            double Square = Math.Round(Calculation.Formula(Convert.ToDouble(radius), Convert.ToDouble(nka)), 3);
            ViewBag.result = Square;
            return View();
        }


    }
}