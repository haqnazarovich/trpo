﻿using System;
using System.Windows;
using TRPO.Lib;

namespace TRPO.WPF
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
            this.WindowStartupLocation = WindowStartupLocation.CenterScreen;
        }


        private void Result_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                txt_Result.Text = Convert.ToString(Calculation.Formula(Convert.ToDouble(txt_Radius.Text), Convert.ToDouble(txt_Nka.Text)));
            }
            catch (FormatException)
            {
                MessageBox.Show("Неверно введено число.");
            }
        }
    }
}
