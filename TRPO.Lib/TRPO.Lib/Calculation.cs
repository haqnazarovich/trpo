﻿using System;

namespace TRPO.Lib
{
    public class Calculation
    {
        public static double Formula(double r, double n)
        {
            if (r <= 0)
            {
                throw new ArgumentException("Переменная r не может быть отрицательной");
            }
            if (n <= 0)
            {
                throw new ArgumentException("Переменная n не может быть отрицательной");
            }
            return Math.Round(Math.PI * Math.Pow(r, 2) * (n / 360), 3);
        }
    }
}
