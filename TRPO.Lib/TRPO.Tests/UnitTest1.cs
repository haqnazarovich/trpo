using NUnit.Framework;
using TRPO.Lib;

namespace TRPO.Tests
{
    public class Tests
    {
        [SetUp]
        public void Setup()
        {
        }

        [Test]
        public void Test1()
        {
            double s = Calculation.Formula(10, 5);
            Assert.AreEqual(4.36, s, 0.01);

        }
        [Test]
        public void Test2()
        {
            double s = Calculation.Formula(10, 5);
            Assert.Throws<System.ArgumentException>(() => {
                double s = Calculation.Formula(-10, 5);
            });
        }
    }
}