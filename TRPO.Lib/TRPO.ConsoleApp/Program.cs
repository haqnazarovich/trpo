﻿using System;
using TRPO.Lib;

namespace TRPO.ConsoleApp
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Введите радиус");
            double radius = Convert.ToDouble(Console.ReadLine());
            Console.WriteLine("Введите высоту");
            double nka = Convert.ToDouble(Console.ReadLine());
            if (radius <= 0)
            {
                Console.WriteLine("радиус не может быть такой...");
            }
            else if (nka <= 0)
            {
                Console.WriteLine("Введите положительную n");
            }
            else
            {
                double answer = Calculation.Formula(radius, nka);
                Console.WriteLine($"{answer}");
            }
            Console.ReadKey();
        }
    }
}
